---
title: "Aster Bedico"
nickname: "Aster "
email: asterbedico@gmail.com
image: "/images/team/aster.jpg"
school: 'College of Saint Benilde'
class: "03"
wallet: "/images/gcashaster.jpg"
jobtitle: "Coordinator, Laguna"
# linkedinurl: "https://www.linkedin.com/in/itsmingjie"
location: "San Pedro, Laguna"
promoted: true
weight: 2
description: Aster is the founder of Angel's Shelter and co-founder of Capri Island Vegan Cafe in Laguna.
---

Aster is the founder of Angel's Shelter and co-founder of Capri Island Vegan Cafe in Laguna.

His passion for healthy eating has led him to connect with Food Rescue and promote the movement in Laguna. He is also active in promoting indigenous culture and local agriculture. 
